#define WS2812_LIB
#include <WS2812_SPI.h>
#include <WS2812_effects.h>
#include <stdio.h>
#include "usb_handlers.h"
#include "config.h"
#include <stdbool.h>
#include <rtc.h>
#include <avr/pgmspace.h>
#include <cdc_descriptors.h>
#include "commandParsing.h"
enum ADALIGHT_STATE{STATE_INIT,ADA_INIT_1,ADA_INIT_2,ADA_LENGTH_H,ADA_LENGTH_L,ADA_CHECK,ADA_DATA_R,ADA_DATA_G,ADA_DATA_B}__attribute__ ((packed));
enum ADALIGHT_STATE curState=STATE_INIT;
volatile uint32_t lastUsbCommand;

void saveCommand(void);
extern uint8_t *led_buffer;

extern struct WS2812_strip leds;
uint16_t adaCurrentIndex, newLength;
void cdc0InHandler(void* data, volatile uint16_t *cnt){
  *cnt = 0;
}
bool cdc0OutHandler(void *data, uint16_t cnt){
  for(uint8_t i=0;i<cnt;i++){
    switch(curState){
    case STATE_INIT:
      if(((uint8_t*)data)[i]=='A') curState=ADA_INIT_1;
      break;
    case ADA_INIT_1:
      lastUsbCommand = millis();
      setLed(leds.buffer, 1, 255,0,0);
      if(((uint8_t*)data)[i]=='d') curState=ADA_INIT_2;
      else curState=STATE_INIT;
      break;
    case ADA_INIT_2:
      if(((uint8_t*)data)[i]=='a') curState=ADA_LENGTH_H;
      else curState=STATE_INIT;
      break;
    case ADA_LENGTH_H:
      newLength = (((uint8_t*)data)[i]) << 8;
      curState = ADA_LENGTH_L;
      break;
    case ADA_LENGTH_L:
      newLength |= (((uint8_t*)data)[i])+1;
      if(leds.num != newLength){
        leds.num = newLength;
        leds.buffer = WS2812_init(&LED_USART, &LED_PORT, &DMA.CH0, LED_PIN, leds.num,led_buffer);
      }
      curState = ADA_CHECK;
      break;
    case ADA_CHECK: //TODO checksum
      curState = ADA_DATA_R;
      adaCurrentIndex=0;
      break;
    case ADA_DATA_R:
      putSPIByte(&leds.buffer[adaCurrentIndex].r,((uint8_t*)data)[i]);
      curState = ADA_DATA_G;
      break;
    case ADA_DATA_G:
      putSPIByte(&leds.buffer[adaCurrentIndex].g,((uint8_t*)data)[i]);
      curState = ADA_DATA_B;
      break;
    case ADA_DATA_B:
      putSPIByte(&leds.buffer[adaCurrentIndex].b,((uint8_t*)data)[i]);
      curState = ADA_DATA_R;
      adaCurrentIndex++;
      if(adaCurrentIndex>=leds.num){
        curState=STATE_INIT;
      }
      break;
    }
  }
  return true;
}
void cdc0SetEncoding(struct cdcLineEncoding_t* enc){}

extern int wait;
bool cdc1OutHandler(void *data, uint16_t cnt){
  for(uint8_t i = 0; i < cnt; i++){
    char c = ((char*)data)[i];
    if( c == '\t'){
      usbBufState = COMMAND_COMPLETION;
      return false;
    }else{
      putc(c,stdout);
      if((c == '\b') && (usbBufIndex > 0)){
        usbBufIndex--;
      }else{
        usbBuf[usbBufIndex] = c;
        usbBufIndex++;
        if( (c == '\r') || (c == '\n') || (usbBufIndex >= sizeof(usbBuf) - 1)){
          putc('\n',stdout);
          usbBuf[usbBufIndex] = 0;
          usbBufState = COMMAND_RECEIVED;
          return false;
        }
      }
    }
  }
  return true;
}
