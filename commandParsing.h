#include <WS2812_SPI.h>
#include <WS2812_effects.h>
#ifndef COMMAND_PARSING_H
#define COMMAND_PARSING_H

#define HIDDEN_COMMAND_MASK 0x80

enum PARSE_RESULT{PARSE_OK, PARSE_CMD_UNKNOWN, PARSE_NO_CMD, PARSE_ARGS_ERROR, PARSE_NUM_ERROR, PARSE_PRINT_STATUS, PARSE_PRINT_EFFECTS, PARSE_INVALID_FORMAT,PARSE_BINARY_MODE};
enum PARSE_RESULT parseAndExecute(char *command, struct WS2812_strip*);
void printEffects(FILE*stream);void switchOff(struct WS2812_strip *strip);
const char* getCommandString(uint8_t command);


struct commandDescriptor{
  uint8_t command;
  const char *string;
};

struct command_completion_result{
  uint8_t num_matches;
  const struct commandDescriptor* first_match;
};

struct command_completion_result completeCommand(char *cmd, uint8_t commandLen);
#endif
