#include <cdc_descriptors.h>
#include <stdbool.h>
#include <stdint.h>
void cdc0InHandler(void*, volatile uint16_t*);
bool cdc0OutHandler(void*, uint16_t);
void cdc0SetEncoding(struct cdcLineEncoding_t* enc);
bool cdc1OutHandler(void*, uint16_t);
volatile char usbBuf[60];
volatile uint8_t usbBufIndex;
enum BUF_STATE {RECEIVING, COMMAND_RECEIVED, COMMAND_COMPLETION}__attribute((packed))__;
volatile enum BUF_STATE usbBufState;
