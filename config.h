#include <baud.h>
#ifndef CONFIG_H
#define CONFIG_H

/**
 * Board specific parameters
 */
#ifdef LEGACY

#elif defined DONGLE

#include <USARTE0.h>
#define LED_USART  USARTC0
#define LED_PORT  PORTC
#define LED_PIN  3
#define BT_USART USARTE0
#define BT_BAUD BAUD_9600
#define BT_STREAM &USARTE0_io
#define BT_USART_INIT(B,E) USARTE0_init(B,E)
#define BT_USART_HEADER USARTE0.h
#define BT_BAUD BAUD_57600
#else

#error "Please define a board!"

#endif

/**
 * Common parameters
 */

#define USB_TIMEOUT 3000
#endif
