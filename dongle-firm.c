
#include <avr/io.h>
#include <util/delay.h>
#include <clock.h>
#include <rtc.h>
#include <avr/eeprom.h>
#include <WS2812_SPI.h>
#include <WS2812_effects.h>
#include <stdio.h>
#include "usb_handlers.h"
#include "usb_config.h"
#include "config.h"
#include <boot.h>
#include <softreset.h>
#include <avr/wdt.h>
#include "commandParsing.h"
#define NO_EEP
#include <init.h>
struct WS2812_strip leds ={
  0,
  0,
  0,
  0,
  0,
  NULL,
  0,
  NULL
};
int timeToNextFrame = 0;
uint8_t lastCommand = 0;
uint8_t led_buffer[1820];
char buf[60];
extern volatile uint32_t lastUsbCommand;
uint32_t lastFrame = 0;
uint8_t bufIndex = 0;

int binaryModeIndex = -1;

const char  usage[] PROGMEM = "Usage:\n\
<effect> <r> <g> <b> [num]\n\
<effect> <brightness> [num]\n\
effects for a list of effects\n\
boot for bootloader\n\
off to turn strip off\n\
status for current settings\n";
void saveCommand(struct WS2812_strip *strip){
  if(leds.cmd!=0xff){
    eeprom_update_block(strip,(void *)0,sizeof(struct WS2812_strip));
  }
}
void restoreCommand(struct WS2812_strip *strip){
  eeprom_read_block(strip,(void*)0,sizeof(struct WS2812_strip));
  strip->buffer = WS2812_init(&LED_USART, &LED_PORT, &DMA.CH0, LED_PIN, strip->num,led_buffer);
}


const char statusFormat[] PROGMEM = "{cmd:\"%S\",r:%hhu,g:%hhu,b:%hhu,num:%u}\n";
static inline void sendStatus(FILE *stream){
  const char * cmd = getCommandString(leds.cmd);
  if(cmd == NULL) cmd = PSTR("none");
  fprintf_P(stream, statusFormat, cmd ,leds.r,leds.g,leds.b,leds.num);
}


void switchOff(struct WS2812_strip *strip){
  timeToNextFrame = -1;
  for(uint16_t i = 0; i <  strip->num; i++){
    setLed(strip->buffer, strip->num - i -1, 0,0,0);
    _delay_ms(50);
  }
}

static inline void printUsage(FILE *stream){
  fputs_P(usage,stdout);
}

static inline void handleUsbInput(void){
  if(usbBufState == COMMAND_COMPLETION){
    struct command_completion_result completion = completeCommand((char*)usbBuf, usbBufIndex);
    switch(completion.num_matches){
    case 1:{
      uint8_t completion_len = strlen_P(pgm_read_ptr(&completion.first_match->string));
      for(; (usbBufIndex < sizeof(usbBuf) -1) && (usbBufIndex < completion_len); usbBufIndex++){
        usbBuf[usbBufIndex] = pgm_read_byte(pgm_read_ptr(&completion.first_match -> string) + usbBufIndex);
        putc(usbBuf[usbBufIndex], stdout);
      }
      break;
    }
    default:
      //puts_P(pgm_read_ptr(&completion.first_match[4].string));
      putc('\n',stdout);
      for(uint8_t i = 0; i < completion.num_matches; i++){
        puts_P(pgm_read_ptr(&completion.first_match[i].string));
        }
      for(uint8_t i = 0; i < usbBufIndex; i++){
        putc(usbBuf[i],stdout);
      }
      break;
      case 0:
      puts("no match");
      break;
    }
    usbBufState = RECEIVING;
    cdcReceive(1);
  }else if(usbBufState == COMMAND_RECEIVED){
    switch(parseAndExecute((char*)usbBuf, &leds)){
    case PARSE_OK:
      break;
    case PARSE_CMD_UNKNOWN:
      break;
    case PARSE_INVALID_FORMAT:
    case PARSE_NO_CMD:
      printUsage(stdout);
      break;
    case PARSE_ARGS_ERROR:
      break;
    case PARSE_PRINT_EFFECTS:
      printEffects(stdout);
      break;
    case PARSE_PRINT_STATUS:
      sendStatus(stdout);
      break;
    }
    usbBufIndex = 0;
    usbBufState = RECEIVING;
    cdcReceive(1);
  }
}
static inline void handleBtInput(void){
  int c = getc(BT_STREAM);
  if(c!= EOF){
    if( binaryModeIndex != -1){
      putSPIByte(((void*)leds.buffer) + (binaryModeIndex*3), (uint8_t) c);
      binaryModeIndex++;
      if(binaryModeIndex == leds.num * 3){
        binaryModeIndex = -1;
      }
    }else{
      putc(c,stdout);
      buf[bufIndex] = c;
      bufIndex++;
      if((c=='\r')||(c == '\n')||(bufIndex == sizeof(buf) - 2)){
        buf[bufIndex] = 0;
        enum PARSE_RESULT res = parseAndExecute((char*)buf, &leds);
        switch(res){
        case PARSE_OK:
          fputc(0x2,BT_STREAM);
          sendStatus(BT_STREAM);
          fputc(0x3,BT_STREAM);
          break;
        case PARSE_CMD_UNKNOWN:
          break;
        case PARSE_INVALID_FORMAT:
        case PARSE_NO_CMD:
          break;
        case PARSE_ARGS_ERROR:
          break;
        case PARSE_PRINT_EFFECTS:
          fputc(0x2,BT_STREAM);
          printEffects(BT_STREAM);
          fputc(0x3,BT_STREAM);
          break;
        case PARSE_PRINT_STATUS:
          fputc(0x2,BT_STREAM);
          sendStatus(BT_STREAM);
          fputc(0x3,BT_STREAM);
          break;
        case PARSE_BINARY_MODE:
          binaryModeIndex = 0;
          timeToNextFrame = -1;
          break;
        }
        bufIndex = 0;
      }
    }
  }
}


static inline void renderFrame(void){
  if((leds.buffer != NULL) && ((millis() - lastUsbCommand) > USB_TIMEOUT)){
    if((leds.cmd != 0) && (((millis() - lastFrame ) >= (uint32_t)timeToNextFrame)) && timeToNextFrame != -1){
      timeToNextFrame = executeEffect(&leds);
      lastFrame = millis();
    }else{
      //_delay_ms(1);
    }
  }else{
    //_delay_ms(1);
  }
}


int main(void){
  if(eeprom_read_byte((uint8_t*)0x7ff) == 0){
    eeprom_write_byte((uint8_t*)0x7ff, 0xff);
    jumpToBootloader();
  }
  init();
  usb_init();
  USARTE0_init(BT_BAUD, false);
  //BT_USART_INIT(BT_BAUD, false);
  stdout = &cdc_io;
  fputs_P("AT+MODE2", &USARTE0_io);
  _delay_ms(500);//timeToNextFrame for possible response
  while(getc(BT_STREAM) != EOF);//flush the response
  //PORTC.PIN3CTRL |= PORT_INVEN_bm;
  if(eeprom_read_byte(0)!=0xff){
    restoreCommand(&leds);
  }
  for(;;){
    handleUsbInput();
    handleBtInput();
    renderFrame();
  }
}
