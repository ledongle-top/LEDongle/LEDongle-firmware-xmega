Firmware for the legacy AVR Xmega-based LEDongle.
Development has stopped in favor of the new ESP32-based LEDongle.
This repo exists mostly for historic purposes, so no further documentation will be created.  

## Overview  
The LEDongle is a compact controller for WS2812 (and compatible) LEDs.  
It can generate different effects on its own or it can be controlled through 
[Hyperion](https://hyperion-project.org/) 
to create ambient lighting and effects.  
The internal effect engine can be controlled through a serial terminal or
(with a BLE serial adapter) through the [companion app](https://gitlab.com/LEDongle/LEDongle-android) (Android only).
Due to the limited memory of the Xmega chip, it supports a maximum of 300 LEDs.  

## Uses  
LEDongle used with Hyperion  
![Ambilight Example](https://i.imgur.com/0ZDeVAx.png)  
LEDongle used for decoration  
![Christmas tree](https://i.imgur.com/SkOaDSP.jpg)  
LEDongle used for PC RGB lighting  
![PC](https://i.imgur.com/U3NxUGF.jpg)  
LEDongle used as an accessory  
![Accessory](https://i.imgur.com/BTkWNpv.jpg)  

## Hardware  
This firmware is written for the Atmel AtXmega32U4 microcontroller. It uses the
controller's USART to generate the signal for the WS2812 in hardware without
bit-banging. It can receive commands and data through its builtin USB interface
(acting as a virtual serial port) or through a serial port (intended to be used
with BLE to serial modules).  
A finished board design including space for a bluetooth module designed to fit
inside the case of the
[cheapest MicroSD reader I could find](https://www.aliexpress.com/item/.../32801767404.html)
can be found [here](https://gitlab.com/schadEigentlich/LEDongle-hardware).  

## Control
Commands can be sent to the dongle through two ways: Either USB or serial.  
When connecting the dongle to a USB port, it will show up as two USB serial adapters
(a driver needs to be installed on Windows, refer to 
[this](http://www.fourwalledcubicle.com/files/LUFA/Doc/140928/html/_page__o_s_drivers.html)
page. The first interface (usually /dev/ttyACM0 on Linux) can be used with the
Hyperion software. See further down for instructions.  
The second interface and the serial port (BLE) interface accept commands for
using the builtin effects.  

## Effect Engine  
The firmware supports many different patterns that work on an arbitrary number
of LEDs, most of them with an adjustable color (excluding randomly colored or 
rainbow effects). Some of the effects were adapted from the 
[NeoPixel Library](https://github.com/adafruit/Adafruit_NeoPixel) by Adafruit.  

## Command Format  
The controller supports two formats for entering commands: a command line style
format and a JSON style format. Both have the same capabilities. An explanation for
the command line style format is available by typing 'help' into the command console.  
Both formats support the following parameters:

| Name   | Type   | Range                           | Description                                                                                                                          |
|--------|--------|---------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| num    | Int    | 0-300                           | The number of LEDs in the current strip                                                                                              |
| r      | Int    | 0-255                           | The red value for the effect color                                                                                                   |
| g      | Int    | 0-255                           | The green value for the effect color                                                                                                 |
| b      | Int    | 0-255                           | The blue value for the effect color                                                                                                  |
| bright | Int    | 0-255                           | The brightness for effects that do not support color. Note: Setting brightness is equivalent to setting r,g and b to the same value. |
| cmd    | String | Any available effect or command | A list of effects can be retrieved with the effects command. Commands: effects, status, off, boot, help                                    |

For the JSON format, one or more parameters can be given as members of a simple 
JSON object.  
For the CLI format, commands can be entered in the following formats:  
`<effect> <r> <g> <b> [num]  
<effect> <brightness> [num]  
<command>`  
The available commands are:  
off: Turns the strip off until a new command is receieved.  
status: Prints the current settings as a JSON object.  
boot: Puts the controller into DFU flashing mode for updating the firmware.  
effects: Prints a list of available effects as a JSON array.  
help: Prints a help text  

## Hyperion Setup  
To use the dongle with Hyperion, configure your Hyperion setup as usual (refer
to the Hyperion documentation or one of the many tutorials online), then choose
Adalight as your protocol, the correct serial port (usually /dev/ttyACM0) and a
standard baud rate such as 115200. This last step actually has no effect on the
communication, however on some systems root privileges are required to use non-standard
baud rates. The USB interface will just run as fast as it can regardless
of the configured baud rate.
When the Hyperion interface and the internal effect engine are used at the same
time, the Hyperion interface will be given priority as long as data is coming in.  