#include <stdio.h>
#include <avr/pgmspace.h>
#include <WS2812_effects.h>
#include <WS2812_SPI.h>
#include <string.h>
#include "dongle_commands.h"
#include "config.h"
#include "commandParsing.h"
#include "dongle_firm.h"
#include <jsmn.h>

static const char string_boot[] PROGMEM = "boot";
static const char string_chase[] PROGMEM = "Chase";
static const char string_christmas[] PROGMEM = "Christmas";
static const char string_colorBlobs[] PROGMEM = "ColorBlobs";
static const char string_colorWipe[] PROGMEM = "ColorWipe";
static const char string_effects[] PROGMEM = "effects";
static const char string_nuigurumi[] PROGMEM = "nuigurumi";
static const char string_off[] PROGMEM = "off";
static const char string_onOffRandom[] PROGMEM = "OnOffRandom";
static const char string_pulse[] PROGMEM = "Pulse";
static const char string_rainbow[] PROGMEM = "Rainbow";
static const char string_rainbowChase[] PROGMEM = "RainbowChase";
static const char string_rainbowCycle[] PROGMEM = "RainbowCycle";
static const char string_rainbowPulse[] PROGMEM = "RainbowPulse";
static const char string_randomPulse[] PROGMEM = "RandomPulse";
static const char string_rawFrame[] PROGMEM = "RawFrame";
static const char string_snake[] PROGMEM = "Snake";
static const char string_solid[] PROGMEM = "Solid";
static const char string_staticRainbow[] PROGMEM = "StaticRainbow";
static const char string_strobe[] PROGMEM = "Strobe";
static const char string_spark[] PROGMEM = "Spark";
static const char string_status[] PROGMEM = "Status";
static const char string_sparkler[] PROGMEM = "Sparkler";
static const char string_wave[] PROGMEM = "Wave";
static const char string_wipeOnOff[] PROGMEM = "WipeOnOff";

void prepareBootloader(void);

//Mapping of strings to command numbers. Must be sorted alphabetically
const struct commandDescriptor commands[] PROGMEM  = {
  {BOOT,string_boot},
  {CHASE,string_chase},
  {CHRISTMAS,string_christmas},
  {COLOR_BLOBS,string_colorBlobs},
  {COLOR_WIPE,string_colorWipe},
  {EFFECTS, string_effects},
  {NUIGURUMI, string_nuigurumi},
  {OFF, string_off},
  {ON_OFF_RANDOM,string_onOffRandom},
  {PULSE, string_pulse},
  {RAINBOW, string_rainbow},
  {RAINBOW_CHASE,string_rainbowChase},
  {RAINBOW_CYCLE, string_rainbowCycle},
  {RAINBOW_PULSE, string_rainbowPulse},
  {RAW_FRAME, string_rawFrame},
  {RANDOM_PULSE, string_randomPulse},
  {SNAKE, string_snake},
  {SOLID, string_solid},
  {SPARK, string_spark},
  {SPARKLER, string_sparkler},
  {STATIC_RAINBOW, string_staticRainbow},
  {STATUS, string_status},
  {STROBE, string_strobe},
  {WAVE,string_wave},
  {WIPE_ON_OFF, string_wipeOnOff}
};
const uint8_t numCommands = sizeof(commands)/sizeof(struct commandDescriptor);
extern uint8_t led_buffer[];

void printEffects(FILE *stream){
  fputc('[',stream);
  uint8_t firstString = 1;
  for(uint8_t i = 0; i < numCommands; i++){
    if(!((uint8_t)pgm_read_byte(&(commands[i].command)) & HIDDEN_COMMAND_MASK)){
      if(!firstString){
        fputs_P(PSTR(",\n"),stream);
      }else{
        firstString = 0;
      }
      fputc('\"', stream);
      fputs_P((char*)pgm_read_ptr(&(commands[i].string)),stream);
      fputc('\"', stream);

      _delay_ms(10);
    }
  }
  /*fputc(']',stream);
    fputc('\n',stream);*/
  fputs_P(PSTR("]\n"),stream);
}

const char* getCommandString(uint8_t command){
  for(uint8_t i = 0; i < numCommands; i++){
    if((uint8_t)pgm_read_byte(&(commands[i].command)) == command){
      return ((char*)pgm_read_ptr(&(commands[i].string)));
    }
  }
  return NULL;
}

static uint8_t matchCommand(char *cmd){
  uint8_t i = 0;
  uint8_t cmp = 0;
  while((i < numCommands) && (cmp = strcasecmp_P(cmd, ((char*)pgm_read_ptr(&commands[i].string)))) > 0){
    i++;
  }
  if(cmp == 0){
    return ((uint8_t)pgm_read_byte(&commands[i].command));
  }else{
    return 0;
  }
}


/**
 * Try to complete a partially entered command
 * command may not be null-terminated
 * return value points to PGMSPACE
 */
struct command_completion_result completeCommand(char *cmd, uint8_t commandLen){
  uint8_t i = 0;
  struct command_completion_result res = {0, NULL};
  while((i < numCommands) && strncasecmp_P(cmd, (char*)pgm_read_ptr(&commands[i].string), commandLen) > 0){
    i++; //skip smaller strings
  }
  res.first_match = &commands[i];//put this here anyway, it shouldnt be used if num_matches == 0
  while((i < numCommands) && strncasecmp_P(cmd, (char*)pgm_read_ptr(&commands[i].string), commandLen) == 0){//count all matches
  res.num_matches+=1;
    i++;
  }
  return res;
}


static uint8_t changeStripNum(struct WS2812_strip *strip, uint16_t num){
  if((num > 300)||(num==0)){
    return 1;
  }
  if(num != strip->num){
    strip->num = num;
    strip->buffer = WS2812_init(&LED_USART, &LED_PORT, &DMA.CH0, LED_PIN, strip->num, led_buffer);
  }
  return 0;
}

static int8_t findToken(char *json,const char *token, jsmntok_t *tokens, uint8_t numTokens){
  char *iterator=json;
  for(uint8_t i=1; i < numTokens; i++){
    iterator = json + tokens[i].start;
    uint8_t len = tokens[i].end - tokens[i].start;
    if(len == strlen_P(token) && !strncasecmp_P(iterator,token, len)){
      return i;
    }
  }
  return -1;
}

uint8_t parseAndExecute(char *command, struct WS2812_strip *strip){
  char *commandString = NULL;
  char *rString = NULL;
  char *gString = NULL;
  char *bString = NULL;
  char *brightString = NULL;
  char *numString = NULL;
  int cmdLength = strnlen(command, 100);
  jsmn_parser parser;
  jsmn_init(&parser);
  jsmntok_t tokens[15];
  int parseResult = jsmn_parse(&parser, command, cmdLength, tokens, 15);
  if(parseResult <= 0){
    return PARSE_INVALID_FORMAT;
  }

  if(command[0] == '{'){//JSON
    int8_t commandToken = findToken(command, PSTR("cmd"), tokens, parseResult);
    uint8_t foundToken;
    if(((foundToken=findToken(command, PSTR("r"), tokens,parseResult)) != -1) && (foundToken < parseResult)){
      rString = command + tokens[foundToken+1].start;
    }
    if(((foundToken=findToken(command, PSTR("g"), tokens,parseResult)) != -1) && (foundToken < parseResult)){
      gString = command + tokens[foundToken+1].start;
    }
    if(((foundToken=findToken(command, PSTR("b"), tokens,parseResult)) != -1) && (foundToken < parseResult)){
      bString = command + tokens[foundToken+1].start;
    }
    if(((foundToken=findToken(command, PSTR("num"), tokens,parseResult)) != -1) && (foundToken < parseResult)){
      numString = command + tokens[foundToken+1].start;
    }
    if(((foundToken=findToken(command, PSTR("bright"), tokens,parseResult)) != -1) && (foundToken < parseResult)){
      brightString = command + tokens[foundToken+1].start;
    }
    commandToken++;
    commandString = command + tokens[commandToken].start;
  }else{//not JSON
    commandString = command+tokens[0].start;
    switch(parseResult){
    case 3:
      numString = command+tokens[2].start;
    case 2:
      brightString = command+tokens[1].start;
      break;
    case 5:
      numString = command+tokens[4].start;
    case 4:
      rString = command+tokens[1].start;
      gString = command+tokens[2].start;
      bString = command+tokens[3].start;
      break;
    case 1:
      break;
    default:
      break;
    }
  }
  for(uint8_t i = 0; i < parseResult; i++){
    command[tokens[i].end]=0;
  }
  uint8_t rawcmd = matchCommand(commandString);
  if(rawcmd & HIDDEN_COMMAND_MASK){//dongle commands
    enum DONGLE_COMMAND cmd = (enum DONGLE_COMMAND) rawcmd;
    switch(cmd){
    case OFF:
      switchOff(strip);
      break;
    case BOOT:
      prepareBootloader();
      break;
    case STATUS:
      return PARSE_PRINT_STATUS;
      break;
    case EFFECTS:
      return PARSE_PRINT_EFFECTS;
      break;
    case RAW_FRAME:
      return PARSE_BINARY_MODE;
      break;
    }
  }else{//strip effects
    if(rawcmd == 0 && rString == NULL && gString == NULL && bString == NULL && numString == NULL && brightString == NULL){
      return PARSE_NO_CMD;
    }
    enum LED_COMMANDS cmd;
    if(rawcmd == 0){
      cmd = strip->cmd;
    }else{
      cmd  = (enum LED_COMMANDS) rawcmd;
    }
    uint8_t r,g,b,brightness;
    uint16_t num;
    if(rString!=NULL){
      r = atoi(rString);
    }else{
      r = strip->r;
    }
    if(gString!=NULL){
      g = atoi(gString);
    }else{
      g = strip->g;
    }
    if(bString!=NULL){
      b = atoi(bString);
    }else{
      b = strip->b;
    }
    if(numString != NULL){
      num = atoi(numString);
      changeStripNum(strip, num);
    }else{
      num = strip->num;
    }
    if(brightString != NULL ){
      brightness = atoi(brightString);
      r = brightness;
      g = brightness;
      b = brightness;
    }
    changeEffect(strip, cmd, r, g ,b);
    saveCommand(strip);
    timeToNextFrame = 0;
  }
  return PARSE_OK;
}
