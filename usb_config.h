#ifndef USB_CONFIG_H
#define USB_CONFIG_H
#include <stdbool.h>
#include <usb.h>
#include <usb_stdio.h>
//1.Configure number of CDC interfaces
#define CDC_INTERFACES 2
#define CDC_OUT_BUFFER_SIZE 16
#define CDC_IN_BUFFER_SIZE 16
#include <usb_datastructs.h>
uint8_t cdc_stdio_if = 1;

//2.Add a handler Function for each function on each interface
void (*cdcInHandlers[CDC_INTERFACES])(void* data, volatile uint16_t *cnt)={
  cdc0InHandler, cdc_stdio_in
};
bool (*cdcOutHandlers[CDC_INTERFACES])(void* data, uint16_t cnt)={
  cdc0OutHandler, cdc1OutHandler
};
void (*cdcEncodingHandlers[CDC_INTERFACES])(struct cdcLineEncoding_t* encoding)={
  cdc0SetEncoding,cdc0SetEncoding
};
#endif
